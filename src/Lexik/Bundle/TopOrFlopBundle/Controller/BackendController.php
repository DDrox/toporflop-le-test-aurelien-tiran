<?php

namespace Lexik\Bundle\TopOrFlopBundle\Controller;

use Lexik\Bundle\TopOrFlopBundle\Entity\Media;
use Lexik\Bundle\TopOrFlopBundle\Form\MediaType;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;

/**
 * Class BackendController
 *
 * @package Lexik\Bundle\TopOrFlopBundle\Controller
 *
 * @Route("/backend")
 */
class BackendController extends Controller
{
    /**
    * @Route("/", name="backend_index")
    *
    * @return Response
    */
    public function indexAction()
    {
        return $this->render('LexikTopOrFlopBundle:Backend:index.html.twig');
    }
    
    /**
     * @Route("/list", name="backend_list")
     *
     * @return Response
     */
    public function showMediasAction()
    {
        $medias = $this->getDoctrine()
                ->getManager()
                ->getRepository('Lexik\Bundle\TopOrFlopBundle\Entity\Media')
                ->findAll();

        return $this->render('LexikTopOrFlopBundle:Backend:list.html.twig', array(
            'medias' => $medias,
        ));
    }
    
    /**
     * @Route("/new", name="backend_new")
     *
     * @param Request $request
     *
     * @return RedirectResponse|Response
     */
    public function newAction(Request $request)
    {
        $media = new Media();

        $form = $this->createForm(new MediaType(), $media);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->get('doctrine.orm.entity_manager');
            $em->persist($media);
            $em->flush();

            return $this->redirect($this->generateUrl('backend_list'));
        }

        return $this->render('LexikTopOrFlopBundle:Backend:new.html.twig', array(
            'form'  => $form->createView(),
        ));
    }
    
    /**
     * @Route("/edit/{id}", name="backend_edit")
     *
     * @param Request $request
     * @param integer $id
     *
     * @return RedirectResponse|Response
     */
    public function editAction(Request $request, $id)
    {
        $media = $this->getDoctrine()
                ->getManager()
                ->getRepository('Lexik\Bundle\TopOrFlopBundle\Entity\Media')
                ->find($id)
        ;

        if (null === $media) {
            throw new NotFoundHttpException("Le média d'id ".$id." n'existe pas.");
        }
        
        $form = $this->createForm(new MediaType(), $media);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->get('doctrine.orm.entity_manager');
            $em->persist($media);
            $em->flush();

            return $this->redirect($this->generateUrl('backend_list'));
        }

        return $this->render('LexikTopOrFlopBundle:Backend:new.html.twig', array(
            'form'  => $form->createView(),
        ));
    }
    
    /**
     * @Route("/del/{id}", name="backend_del")
     *
     * @param Request $request
     * @param integer $id
     *
     * @return RedirectResponse|Response
     */
    public function delAction(Request $request, $id)
    {
        $em = $this->getDoctrine()->getManager();
        
        $media = $em->getRepository('Lexik\Bundle\TopOrFlopBundle\Entity\Media')->find($id);

        if (null === $media) {
            throw new NotFoundHttpException("Le média d'id ".$id." n'existe pas.");
        }

        $form = $this->createFormBuilder()->getForm();

        if ($form->handleRequest($request)->isValid()) {
            $em->remove($media);
            $em->flush();

            return $this->redirect($this->generateUrl('backend_list'));
        }

        return $this->render('LexikTopOrFlopBundle:Backend:del.html.twig', array(
            'media'  => $media,
            'form'  => $form->createView(),
        ));
    }
}
