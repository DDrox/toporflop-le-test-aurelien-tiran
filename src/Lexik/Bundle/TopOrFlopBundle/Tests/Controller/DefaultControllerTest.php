<?php

namespace Lexik\Bundle\TopOrFlopBundle\Tests\Controller;

use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;

/**
 * DefaultController tests
 */
class DefaultControllerTest extends WebTestCase
{
    public function testIndex()
    {
        $client = static::createClient();

        $crawler = $client->request('GET', '/');

        $this->assertGreaterThan(0, $crawler->filter('html:contains("Bienvenue sur ToporFlops !")')->count());
    }

    public function testVoingMessage()
    {
        $client = static::createClient();
        $client->followRedirects();

        $crawler = $client->request('GET', '/');

        // go to a media page anonymously
        $mediaLink = $crawler->filter('a:contains("Média au hasard")')->eq(0)->link();
        $crawler = $client->click($mediaLink);

        // a message should tell me to login to vote
        $this->assertEquals(1, $crawler->filter('html:contains("Vous devez être connecté pour voter.")')->count());

        // login
        $loginLink = $crawler->filter('a:contains("Se connecter")')->eq(0)->link();
        $crawler = $client->click($loginLink);

        $form = $crawler->selectButton('Connexion')->form();
        $form['_username'] = 'jeremy';
        $form['_password'] = 'password';

        $crawler = $client->submit($form);

        // go to a media page authenticated
        $mediaLink = $crawler->filter('a:contains("Média au hasard")')->eq(0)->link();
        $crawler = $client->click($mediaLink);

        // the message shouldn't be here anymore
        $this->assertEquals(0, $crawler->filter('html:contains("Vous devez être connecté pour voter.")')->count());
    }
}
