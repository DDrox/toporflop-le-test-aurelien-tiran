<?php

namespace Lexik\Bundle\TopOrFlopBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;

class VoteType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('score', 'choice', array(
                'choices' => array_combine(range(1, 10), range(1, 10)),
            ))
            ->add('save', 'submit')
        ;
    }

    public function setDefaultOptions(OptionsResolverInterface $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'Lexik\Bundle\TopOrFlopBundle\Entity\Vote'
        ));
    }

    public function getName()
    {
        return 'lexik_bundle_toporflopbundle_votetype';
    }
}
