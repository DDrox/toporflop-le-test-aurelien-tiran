<?php

namespace Lexik\Bundle\TopOrFlopBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * Class Vote
 *
 * @package Lexik\Bundle\TopOrFlopBundle\Entity
 *
 * @ORM\Table(
 *     uniqueConstraints={@ORM\UniqueConstraint(name="vote_unique_idx", columns={"user_id", "media_id"})}
 * )
 * @ORM\Entity()
 */
class Vote
{
    /**
     * @var integer $id
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var integer $score
     *
     * @ORM\Column(name="score", type="integer")
     *
     * @Assert\NotBlank()
     */
    private $score;

    /**
     * @var \DateTime $date
     *
     * @ORM\Column(name="date", type="datetime")
     *
     * @Assert\NotBlank()
     * @Assert\DateTime()
     */
    private $date;

    /**
     * @var User $user
     *
     * @ORM\ManyToOne(
     *     targetEntity="Lexik\Bundle\TopOrFlopBundle\Entity\User",
     *     inversedBy="votes"
     * )
     * @ORM\JoinColumn(
     *     name="user_id",
     *     referencedColumnName="id"
     * )
     */
    private $user;

    /**
     * @var Media $media
     *
     * @ORM\ManyToOne(
     *     targetEntity="Lexik\Bundle\TopOrFlopBundle\Entity\Media",
     *     inversedBy="votes",
     *     cascade={"persist"}
     * )
     * @ORM\JoinColumn(
     *     name="media_id",
     *     referencedColumnName="id",
     *     onDelete="CASCADE"
     * )
     */
    private $media;

    /**
     * Constructor
     */
    public function __construct()
    {
        $this->date = new \DateTime('now');
    }

    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set score
     *
     * @param integer $score
     */
    public function setScore($score)
    {
        $this->score = $score;
    }

    /**
     * Get score
     *
     * @return integer
     */
    public function getScore()
    {
        return $this->score;
    }

    /**
     * Set date
     *
     * @param  \DateTime $date
     */
    public function setDate($date)
    {
        $this->date = $date;
    }

    /**
     * Get date
     *
     * @return \DateTime
     */
    public function getDate()
    {
        return $this->date;
    }

    /**
     * Set user
     *
     * @param User $user
     */
    public function setUser(User $user)
    {
        $this->user = $user;
    }

    /**
     * Get user
     *
     * @return User
     */
    public function getUser()
    {
        return $this->user;
    }

    /**
     * Set media
     *
     * @param Media $media
     */
    public function setMedia(Media $media)
    {
        $this->media = $media;
    }

    /**
     * Get media
     *
     * @return Media
     */
    public function getMedia()
    {
        return $this->media;
    }
}
