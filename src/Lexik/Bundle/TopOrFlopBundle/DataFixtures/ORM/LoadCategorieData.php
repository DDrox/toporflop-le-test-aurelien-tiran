<?php

namespace Lexik\Bundle\TopOrFlopBundle\DataFixtures\ORM;

use Doctrine\Common\DataFixtures\AbstractFixture;
use Doctrine\Common\DataFixtures\OrderedFixtureInterface;
use Doctrine\Common\Persistence\ObjectManager;
use Lexik\Bundle\TopOrFlopBundle\Entity\Categorie;

/**
 * Class LoadCategorieData
 *
 * @package Lexik\Bundle\TopOrFlopBundle\DataFixtures\ORM
 */
class LoadCategorieData extends AbstractFixture implements OrderedFixtureInterface
{
    /**
     * {@inheritdoc}
     */
    public function load(ObjectManager $manager)
    {
        $noms = array(
            'chat',
            'web',
            'lexik',
        );

        foreach ($noms as $nom) {
            $categorie = new Categorie();
            $categorie->setNom($nom);
            
            $manager->persist($categorie);

            $this->addReference(
                sprintf('categorie-%s', $nom),
                $categorie
            );
        }

        $manager->flush();
    }

    /**
     * {@inheritdoc}
     */
    public function getOrder()
    {
        return 5;
    }
}
